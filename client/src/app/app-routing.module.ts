import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { SearchEngineComponent } from './search-engine/search-engine.component';

const routes: Routes = [
  {path: '', component: HomepageComponent },
  {path:'searchPage', component : SearchEngineComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
